from django.conf import settings
from appconf import AppConf


class ContentManagerConf(AppConf):
    PARAMETER = "editmode"
    SESSION_KEY = "contentmanager_editmode"
    KLASSES = None

    class Meta:
        prefix = 'content_manager'
