from .conf import settings
from .utils import has_permissions


class EditmodeMiddleware(object):
    """
    Change editmode in session.
    """

    def __init__(self):
        self.parameter = settings.CONTENTMANAGER_PARAMETER
        self.sessionkey = settings.CONTENTMANAGER_SESSION_KEY

    def _set_editmode(self, request, current):
        # toggle switch if needed
        if request.GET[self.parameter] == '1' and not current:
            request.session[self.sessionkey] = True

        elif request.GET[self.parameter] == '0' and current:
            request.session[self.sessionkey] = False

    def process_request(self, request):
        perm = has_permissions(request.user)
        current = request.session.get(self.sessionkey, False)

        # toggle editmode flag if asked for and user has permission.
        if perm and self.parameter in request.GET:
            self._set_editmode(request, current)

        # remove flag if not logged in and currently set to true
        if not perm and current:
            request.session[self.sessionkey] = False
