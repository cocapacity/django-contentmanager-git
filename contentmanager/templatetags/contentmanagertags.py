from django import template
from django.db.models import Min
from django.contrib.auth.models import AnonymousUser
from django.core.cache import cache
from django.http import HttpRequest

from contentmanager.registry import NotRegistered
from contentmanager.models import Block, BlockPath, Area
from contentmanager.utils import get_area_ckey, has_permissions


register = template.Library()


class AreaNode(template.Node):
    """

    """

    def __init__(self, request, area, path=None, small=False):
        self.request = template.Variable(request)
        self.area = template.Variable(area)
        self.small = small
        if path is not None:
            self.path = template.Variable(path)
        else:
            self.path = None

    def render(self, context):
        request = self.request.resolve(context)
        area = self.area.resolve(context)

        # path not set use request.path
        if self.path is not None:
            path = self.path.resolve(context)
        else:
            path = request.path

        # get path and area objects
        area = Area.objects.get_for_area(area)
        path = BlockPath.objects.get_for_path(path)

        # get blocks lazy
        blocks = Block.objects.get_for_area_path(area,
                                                 path).order_by('position')

        # get cache key
        ckey = get_area_ckey(area, path)

        # Never cache in editmode or when 1 block is not cacheable
        editmode = request.session.get('contentmanager_editmode', False)
        cacheable = bool(blocks.aggregate(cacheable=Min('cacheable'))['cacheable'])
        content = None

        if editmode:
            templatename = 'contentmanager/contentmanager.html'
        else:
            # only used when there's no content in the cache
            templatename = 'contentmanager/content.html'
            if cacheable:
                content = cache.get(ckey)

        # if the cache has content we exit now
        if content:
            return content

        # render content
        context = template.RequestContext(request, {'blocks': blocks,
                                                    'area': area,
                                                    'small': self.small,
                                                    'path': path})

        content = template.loader.render_to_string(templatename, context)

        # store the area's content in cache... except when not
        if not editmode and cacheable:
            cache.set(ckey, content)
        return content


@register.tag
def get_area(parser, token):
    """
    Render all plugins in a area, with optional fixed path.

    {% get_area <request> <area> <small>%}
    {% get_area <request> <area> for <path> <small> %}
    """
    bits = token.split_contents()
    small = False
    if len(bits) in [4, 6]:
        small = str(bits.pop()).lower() == "small"
    if len(bits) != 3 and len(bits) != 5:
        raise template.TemplateSyntaxError(
            "incorrect get_area format")

    if len(bits) == 5:
        if bits[3] != 'for':
            raise template.TemplateSyntaxError(
                "third argument to the get_area tag must be 'for'")
        return AreaNode(bits[1], bits[2], bits[4], small)
    return AreaNode(bits[1], bits[2], small=small)


class BlockNode(template.Node):
    def __init__(self, request, block, var_name):
        self.request = template.Variable(request)
        self.block = template.Variable(block)
        self.var_name = var_name

    def render(self, context):
        try:
            request = self.request.resolve(context)
        except template.VariableDoesNotExist:
            # This is here mostly for haystack/whoosh
            request = HttpRequest()
            request.user = AnonymousUser()
        block = self.block.resolve(context)

        def inner(output):
            if self.var_name is not None:
                context[self.var_name] = output
                return ''
            return output

        # get plugin
        try:
            plugin = block.get_plugin()
        except NotRegistered, error:
            from django.conf import settings

            if settings.DEBUG:
                return inner(repr(error))
            else:
                return inner('')
            # render
        try:
            return inner(plugin.render(request))
        except Exception, error:
            from django.conf import settings

            if settings.DEBUG:
                return inner(repr(error))
            else:
                return inner("<!-- plugin '" + plugin.verbose_name + "' failed to render -->")


@register.tag
def render_block(parser, token):
    """
    {% render_block request block %}
    """
    bits = token.split_contents()

    if len(bits) != 3 and len(bits) != 5:
        raise template.TemplateSyntaxError(
            "render_block tag takes exactly two or four arguments")
    if len(bits) == 5:
        return BlockNode(bits[1], bits[2], bits[4])
    return BlockNode(bits[1], bits[2], None)


@register.assignment_tag
def has_content_permissions(request):
    """
    Checks if user has any permissions for a block.
    """
    return has_permissions(request.user)


@register.assignment_tag
def has_permissions_for(request, block):
    """
    Checks if user has specific permissions for a block.
    """
    return block.get_plugin().has_permission(request)
