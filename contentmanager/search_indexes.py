from models import Block

# Don't die when haystack is not available
try:
    from haystack import indexes

    class BlockIndex(indexes.SearchIndex, indexes.Indexable):
        text = indexes.CharField(document=True, use_template=True,
                                 template_name=['contentmanager/search/indexes/block_text.txt',
                                                'search/indexes/contentmanager/block_text.txt'])
        # rendered contains a standard search result (<h3><a href="url to object">plugin_type</a></h3>)
        rendered = indexes.CharField(use_template=True, indexed=False,
                                     template_name=['contentmanager/search/indexes/block_rendered.txt',
                                                    'search/indexes/contentmanager/block_rendered.txt'])

        def index_queryset(self):
            """Not all blocks should be indexed, this should be column set by
               the plugin for now only paragraph."""
            return Block.objects.filter(plugin_type__name="Paragraph")

except ImportError:
    pass
