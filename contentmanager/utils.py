from django.core.cache import cache
from django.conf import settings


def get_area_ckey(area, path):
    """
    Get area key based on current site, area and path

    area and path can be there respected object, pk or string.
    """
    from models import Area, BlockPath

    if isinstance(area, int):
        pass
    elif isinstance(area, Area):
        area = area.pk
    else:
        area = Area.objects.get_for_area(area).pk

    if isinstance(path, int):
        pass
    elif isinstance(path, BlockPath):
        path = path.pk
    else:
        path = BlockPath.objects.get_for_path(path).pk

    return u'contentmanager:%d:%d:%d' % (settings.SITE_ID, area, path)


def expire_cache(area, path):
    """
    Expires the cache for current site, area/path combination.
    """
    cache.delete(get_area_ckey(area, path))


def has_permissions(user):
    # Inactive users have no permission
    if not user.is_active:
        return False

    # Superusers have all permissions.
    if user.is_superuser:
        return True

    # now check if we have any plugin permissions
    for perm in user.get_all_permissions():
        if perm[:perm.index('.')] == "contentmanager" and perm.endswith("_plugin"):
            return True

    # no permission, to bad for you
    return False


def has_permission_for(user, plugin):
    # Inactive users have no permission
    if not user.is_active:
        return False

    # Superusers have all permissions.
    if user.is_superuser:
        return True

    # now check if we have any plugin permissions
    for perm in user.get_all_permissions():
        if perm[:perm.index('.')] == "contentmanager" and perm.endswith("_plugin"):
            return True

    # no permission, to bad for you
    return False
