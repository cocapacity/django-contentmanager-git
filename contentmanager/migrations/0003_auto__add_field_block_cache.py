# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Block.cacheable'
        db.add_column('contentmanager_block', 'cacheable',
                      self.gf('django.db.models.fields.IntegerField')(default=0, db_index=True),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Block.cacheable'
        db.delete_column('contentmanager_block', 'cacheable')


    models = {
        'contentmanager.area': {
            'Meta': {'unique_together': "(('name', 'site'),)", 'object_name': 'Area'},
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'site': ('django.db.models.fields.related.ForeignKey', [], {'default': '1', 'related_name': "'areas'", 'to': "orm['sites.Site']"})
        },
        'contentmanager.block': {
            'Meta': {'object_name': 'Block'},
            'area': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'blocks'", 'to': "orm['contentmanager.Area']"}),
            'cacheable': ('django.db.models.fields.IntegerField', [], {'default': '0', 'db_index': 'True'}),
            'created': ('django.db.models.fields.DateTimeField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'klass': ('django.db.models.fields.CharField', [], {'max_length': '250', 'blank': 'True'}),
            'params': ('contentmanager.fields.PickledObjectField', [], {'null': 'True', 'blank': 'True'}),
            'path': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'blocks'", 'to': "orm['contentmanager.BlockPath']"}),
            'plugin_type': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'blocks'", 'to': "orm['contentmanager.PluginType']"}),
            'position': ('django.db.models.fields.PositiveSmallIntegerField', [], {'db_index': 'True'}),
            'site': ('django.db.models.fields.related.ForeignKey', [], {'default': '1', 'related_name': "'blocks'", 'to': "orm['sites.Site']"}),
            'updated': ('django.db.models.fields.DateTimeField', [], {'db_index': 'True'})
        },
        'contentmanager.blockpath': {
            'Meta': {'unique_together': "(('path', 'site'),)", 'object_name': 'BlockPath'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'path': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'site': ('django.db.models.fields.related.ForeignKey', [], {'default': '1', 'related_name': "'blockpaths'", 'to': "orm['sites.Site']"})
        },
        'contentmanager.plugintype': {
            'Meta': {'object_name': 'PluginType'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255'})
        },
        'sites.site': {
            'Meta': {'ordering': "('domain',)", 'object_name': 'Site', 'db_table': "'django_site'"},
            'domain': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        }
    }

    complete_apps = ['contentmanager']