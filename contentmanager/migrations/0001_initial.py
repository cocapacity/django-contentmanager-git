# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Area'
        db.create_table('contentmanager_area', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('site', self.gf('django.db.models.fields.related.ForeignKey')(default=1, related_name='areas', to=orm['sites.Site'])),
            ('description', self.gf('django.db.models.fields.TextField')(blank=True)),
        ))
        db.send_create_signal('contentmanager', ['Area'])

        # Adding unique constraint on 'Area', fields ['name', 'site']
        db.create_unique('contentmanager_area', ['name', 'site_id'])

        # Adding model 'PluginType'
        db.create_table('contentmanager_plugintype', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(unique=True, max_length=255)),
        ))
        db.send_create_signal('contentmanager', ['PluginType'])

        # Adding model 'BlockPath'
        db.create_table('contentmanager_blockpath', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('path', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('site', self.gf('django.db.models.fields.related.ForeignKey')(default=1, related_name='blockpaths', to=orm['sites.Site'])),
        ))
        db.send_create_signal('contentmanager', ['BlockPath'])

        # Adding unique constraint on 'BlockPath', fields ['path', 'site']
        db.create_unique('contentmanager_blockpath', ['path', 'site_id'])

        # Adding model 'Block'
        db.create_table('contentmanager_block', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('plugin_type', self.gf('django.db.models.fields.related.ForeignKey')(related_name='blocks', to=orm['contentmanager.PluginType'])),
            ('site', self.gf('django.db.models.fields.related.ForeignKey')(default=1, related_name='blocks', to=orm['sites.Site'])),
            ('area', self.gf('django.db.models.fields.related.ForeignKey')(related_name='blocks', to=orm['contentmanager.Area'])),
            ('path', self.gf('django.db.models.fields.related.ForeignKey')(related_name='blocks', to=orm['contentmanager.BlockPath'])),
            ('position', self.gf('django.db.models.fields.PositiveSmallIntegerField')(db_index=True)),
            ('klass', self.gf('django.db.models.fields.CharField')(max_length=250, blank=True)),
            ('params', self.gf('contentmanager.fields.PickledObjectField')(null=True, blank=True)),
            ('created', self.gf('django.db.models.fields.DateTimeField')()),
            ('updated', self.gf('django.db.models.fields.DateTimeField')(db_index=True)),
        ))
        db.send_create_signal('contentmanager', ['Block'])


    def backwards(self, orm):
        # Removing unique constraint on 'BlockPath', fields ['path', 'site']
        db.delete_unique('contentmanager_blockpath', ['path', 'site_id'])

        # Removing unique constraint on 'Area', fields ['name', 'site']
        db.delete_unique('contentmanager_area', ['name', 'site_id'])

        # Deleting model 'Area'
        db.delete_table('contentmanager_area')

        # Deleting model 'PluginType'
        db.delete_table('contentmanager_plugintype')

        # Deleting model 'BlockPath'
        db.delete_table('contentmanager_blockpath')

        # Deleting model 'Block'
        db.delete_table('contentmanager_block')


    models = {
        'contentmanager.area': {
            'Meta': {'unique_together': "(('name', 'site'),)", 'object_name': 'Area'},
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'site': ('django.db.models.fields.related.ForeignKey', [], {'default': '1', 'related_name': "'areas'", 'to': "orm['sites.Site']"})
        },
        'contentmanager.block': {
            'Meta': {'object_name': 'Block'},
            'area': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'blocks'", 'to': "orm['contentmanager.Area']"}),
            'created': ('django.db.models.fields.DateTimeField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'klass': ('django.db.models.fields.CharField', [], {'max_length': '250', 'blank': 'True'}),
            'params': ('contentmanager.fields.PickledObjectField', [], {'null': 'True', 'blank': 'True'}),
            'path': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'blocks'", 'to': "orm['contentmanager.BlockPath']"}),
            'plugin_type': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'blocks'", 'to': "orm['contentmanager.PluginType']"}),
            'position': ('django.db.models.fields.PositiveSmallIntegerField', [], {'db_index': 'True'}),
            'site': ('django.db.models.fields.related.ForeignKey', [], {'default': '1', 'related_name': "'blocks'", 'to': "orm['sites.Site']"}),
            'updated': ('django.db.models.fields.DateTimeField', [], {'db_index': 'True'})
        },
        'contentmanager.blockpath': {
            'Meta': {'unique_together': "(('path', 'site'),)", 'object_name': 'BlockPath'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'path': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'site': ('django.db.models.fields.related.ForeignKey', [], {'default': '1', 'related_name': "'blockpaths'", 'to': "orm['sites.Site']"})
        },
        'contentmanager.plugintype': {
            'Meta': {'object_name': 'PluginType'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255'})
        },
        'sites.site': {
            'Meta': {'ordering': "('domain',)", 'object_name': 'Site', 'db_table': "'django_site'"},
            'domain': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        }
    }

    complete_apps = ['contentmanager']