from django.contrib import admin
from django.db.models import Count
from models import Area, Block, PluginType, BlockPath


class AreaAdmin(admin.ModelAdmin):
    pass


admin.site.register(Area, AreaAdmin)


class PluginTypeAdmin(admin.ModelAdmin):
    pass


admin.site.register(PluginType, PluginTypeAdmin)


class BlockPathAdmin(admin.ModelAdmin):
    list_display = ('path', 'num_blocks')
    search_fields = ('path', )

    def queryset(self, request):
        qs = super(BlockPathAdmin, self).queryset(request)
        return qs.annotate(block_count=Count('blocks'))

    def num_blocks(self, obj):
        return obj.block_count

    num_blocks.short_description = 'Blocks'
    num_blocks.admin_order_field = 'block_count'


admin.site.register(BlockPath, BlockPathAdmin)


class BlockAdmin(admin.ModelAdmin):
    list_display = ('pk', 'area', 'path', 'position', 'plugin_type')
    list_filter = ('area', )
    search_fields = ('path__path', 'plugin_type__name' )


admin.site.register(Block, BlockAdmin)
