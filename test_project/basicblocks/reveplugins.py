from django import forms
from django.template import Template
from django.template.loader import render_to_string
from django.utils.translation import ugettext as _

from contentmanager import registry
from contentmanager.plugins import BasePlugin


class BackToTop(BasePlugin):
    def render(self, request):
        return render_to_string("basicblocks/backtotop.html")

class ParagraphForm(forms.Form):
    title = forms.CharField(max_length=255, required=False)
    text = forms.CharField(widget=forms.Textarea)


class ParagraphPlugin(BasePlugin):
    form = ParagraphForm
    verbose_name = 'Paragraph'

    def render(self, request):
        return render_to_string('basicblocks/paragraph.html',
                                {'title': self.params.get("title"),
                                 'text': self.params.get("text")})

## registry ##
registry.register(ParagraphPlugin)
registry.register(BackToTop)
