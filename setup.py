import os

from setuptools import setup, find_packages


def read(filename):
    return open(os.path.join(os.path.dirname(__file__), filename)).read()


install_requires = [
    'django-appconf',
    'south',
    'django-haystack',
    'Whoosh',
],

setup(
    name='django-contentmanager',
    version='13.04',

    url='http://bitbucket.org/cocapacity/django-contentmanager/',
    description='A simple, pluggable content-manager for django.',
    long_description=read('README.rst'),

    license='BSD',
    author='Co-Capacity',
    author_email='django@co-capacity.org',
    packages=find_packages(),
    include_package_data=True,
    install_requires=install_requires,
    zip_safe=False,

    classifiers=[
        'Development Status :: 4 - Beta',
        'Environment :: Web Environment',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Framework :: Django',
    ]
)
